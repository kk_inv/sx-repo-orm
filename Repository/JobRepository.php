<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Job;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 *
 * @author Kkos
 */
class JobRepository extends EntityRepository
{

    function findAdminOpenJobs($page, $limit, $sortBy, $orderBy, $filters)
    {
        $q = "SELECT 
				j as job,
				u.username, c.company_name,  
				pt.name as pickup_town,
				dt.name as delivery_town, 
				q.amount as quote_amount, 
				COUNT(q.quote_id) as quote_count, 
				js.name as job_status_name, 
				js.status_id as job_status_id,
				j.cancelled
				FROM AppBundle:Job j 
				LEFT JOIN j.pickup_town pt
				LEFT JOIN j.delivery_town dt
				INNER JOIN j.status js
				INNER JOIN j.booker u 
				INNER JOIN u.client c
				INNER JOIN AppBundle:Quote q WITH (q.job = j)
				WHERE j.status IN ('1') AND j.cancelled = false
				";
        
        $q .= " GROUP BY j.job_id ORDER BY j.{$sortBy} {$orderBy} ";
        
        $qb = $this->getEntityManager()->createQuery($q);
               
        return $paginator = $this->paginate($qb, $page, $limit);
    }

    public function findProviderMyJobs($page, $limit, $user_provider, $sortBy, $orderBy, $filters)
    {
        $q = "SELECT 
				j as job, pt.name as pickup_town, 
				dt.name as delivery_town, 
				q.amount as quote_amount, 
				0 as quote_count, 
				js.name as job_status_name, 
				js.status_id as job_status_id, 
				j.cancelled 
				FROM AppBundle:Job j 
				LEFT JOIN j.pickup_town pt
				LEFT JOIN j.delivery_town dt
				LEFT JOIN j.status js
				INNER JOIN AppBundle:Quote q WITH (q.status=100 AND q.job = j AND q.user=:user)
				WHERE j.job_id >= 0 AND j.status >= 30
				";
        
        
        $q .= " ORDER BY j.{$sortBy} {$orderBy} ";
        
        $qb = $this->getEntityManager()
            ->createQuery($q)
            ->setParameter('user', $user_provider);
        
        return $paginator = $this->paginate($qb, $page, $limit);
    }

    public function findDriverActiveJobs($driver)
    {
        $q = "SELECT j 
				FROM AppBundle:Job j 
				LEFT JOIN j.pickup_town pt
				LEFT JOIN j.delivery_town dt
				LEFT JOIN j.status js
				WHERE j.driver = :driver AND j.status > 30 AND j.status<100 AND j.cancelled !=1 ";
        
        $qb = $this->getEntityManager()
            ->createQuery($q)
            ->setParameter('driver', $driver)
            ->getResult();
        return $qb;
    }

    public function countQuotes($job)
    {
        $dql = "SELECT
				COUNT(q.quote_id)
				FROM AppBundle:Quote q
				WHERE q.job = :job";
        
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('job', $job);
        
        return $query->getResult()[0][1];
    }

    public function findProvidersInterestedInNewJob($job)
    {
        $stateFrom = $job->getPickupTown()->getState();
        $stateTo = $job->getDeliveryTown()->getState();
        
        $dql = "SELECT u
				FROM AppBundle:User u
				JOIN u.provider p
				WHERE u.id NOT IN (
					SELECT user.id
					FROM AppBundle:User user
					JOIN user.deselects rd
					WHERE (rd.from_to = 0 AND rd.state_name = :stateFrom) OR (rd.from_to = 1 AND rd.state_name = :stateTo)
				)
				GROUP BY u.id
				";
        
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameters(array(
            'stateFrom' => $stateFrom,
            'stateTo' => $stateTo
        ));
        
        return $query->getResult();
    }

    public function findProviderForJob($job)
    {
        $dql = "SELECT u
				FROM AppBundle:User u
				JOIN u.quotes q
				JOIN q.job j
				WHERE j = :job AND j.status > 10
				";
        
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('job', $job);
        
        if (isset($query->getResult()[0]))
            return $query->getResult()[0];
        else
            return 0;
    }

    public function getJobsTodayExpire()
    {
        $q = "SELECT j
				FROM AppBundle:Job j
				WHERE j.expiry_date = CURRENT_DATE() ";
        
        $qb = $this->getEntityManager()
            ->createQuery($q)
            ->getResult();
        return $qb;
    }

    public function findQuotesForJob($job)
    {
        $dql = "SELECT q
				FROM AppBundle:Quote q
				JOIN q.job j
				WHERE j = :job
				";
        
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('job', $job);
        
        return $query->getResult();
    }

    public function paginate($dql, $page = 1, $limit = 5)
    {
        $paginator = new Paginator($dql);
        
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);
        
        return $paginator;
    }
}

