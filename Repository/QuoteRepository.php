<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * @author Kkos
*/

class QuoteRepository extends EntityRepository
{

	/**
	 * @return Query
	 */
	public function getQuoteJob($q)
	{

		$statement = $this->getEntityManager()->getConnection()->prepare("
                SELECT q.* 
                FROM sx_quotes q
                WHERE q.job_id = :job_id
                ORDER BY q.quote_id DESC
            ");
		$statement->bindValue('job_id', $q);
		$statement->execute();
		return $statement->fetchAll();
		
	}
	
	public function findQuotesProvider($user_provider)
	{
	
		$q = "SELECT q as quote, 
				j.job_id as job_id, j.title as job_title
				FROM AppBundle:Quote q
				INNER JOIN q.job j
				WHERE q.user=:user
				ORDER BY q.quote_id DESC
				";
	
		return $qb = $this->getEntityManager()->createQuery($q)
		->setParameter('user', $user_provider)->getResult();

	}
	
}
