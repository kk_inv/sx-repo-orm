<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 *
 * @author Kkos
 *        
 */
class UserRepository extends EntityRepository {
	
	public function getAdminEmails() {
		return $this->getEntityManager ()->createQuery ( "SELECT u.email from AppBundle:User u WHERE u.roles LIKE '%ROLE_ADMIN%'" )->getResult ();
	}
	

	
	public function findUsersPaginatedClient($page, $sortBy, $orderBy, $limit, $client) {
		$q = "SELECT u as user, c.company_name as company_name 
				FROM AppBundle:User u 
				INNER JOIN AppBundle:Client c WITH (u.client = :client AND u.client = c)
				WHERE 1=1 
				";
		
		$q .= " GROUP BY u.id ORDER BY u.{$sortBy} {$orderBy} ";
		$qb = $this->getEntityManager ()->createQuery ( $q );
		$qb->setParameter ( 'client', $client );
		
		return $paginator = $this->paginate ( $qb, $page, $limit );
	}
	
	
	public function paginate($dql, $page = 1, $limit = 5) {
		$paginator = new Paginator ( $dql );
		$paginator->getQuery ()->setFirstResult ( $limit * ($page - 1) )-> setMaxResults ( $limit ); 
		return $paginator;
	}
	
}
