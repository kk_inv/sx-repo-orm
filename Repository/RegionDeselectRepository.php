<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Kkos
 */

class RegionDeselectRepository extends EntityRepository
{
	public function getDeselectedStatesForProvider($user)
	{
		$q = "SELECT rd
				FROM AppBundle:RegionDeselect rd
				WHERE rd.provider = :user
				";
	
		$query = $this->getEntityManager()->createQuery($q)->setParameter('user', $user);
	
		return $query->getResult();
	}

}