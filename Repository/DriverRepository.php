<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 *
 * @author Kkos
 *        
 */
class DriverRepository extends EntityRepository
{

    function findDriversAdmin($filters, $page, $sortBy, $orderBy, $limit)
    {
        if (isset($filters['search']))
            $search = $filters['search'];
        
        $q = "SELECT
				d as driver, p.company_name as company_name 
				FROM AppBundle:Driver d
				INNER JOIN d.provider p 
				WHERE d.provider = p ";
        
        if (! empty($search))
            $q .= " AND (CONCAT(d.forename,' ',d.surname) LIKE :search OR d.username LIKE :search OR d.email LIKE :search) ";
        
        $q .= " GROUP BY d.driver_id ORDER BY d.{$sortBy} {$orderBy} ";
        
        $qb = $this->getEntityManager()->createQuery($q);
        
        if (! empty($search))
            $qb->setParameter('search', '%' . $search . '%');
        
        return $paginator = $this->paginate($qb, $page, $limit);
    }

    function findDriversProvider($page, $sortBy, $orderBy, $limit, $provider)
    {
        
        $q = "SELECT
				d as driver, p.company_name as company_name
				FROM AppBundle:Driver d
				INNER JOIN AppBundle:Provider p WITH (d.provider = :provider)
				WHERE d.provider = p ";
        
        if (! empty($search))
            $q .= " AND (CONCAT(d.forename,' ',d.surname) LIKE :search OR d.username LIKE :search OR d.email LIKE :search) ";
        
        $q .= " GROUP BY d.driver_id ORDER BY d.{$sortBy} {$orderBy} ";
        
        $qb = $this->getEntityManager()->createQuery($q);
        
        $qb->setParameter('provider', $provider);
        
        return $paginator = $this->paginate($qb, $page, $limit);
    }
    
    // FOR MOBILE APP API ONLY
    function findDriversCompany($provider, $driver_excluded)
    {
        $q = "SELECT
				d.driver_id as driver_id, d.username as username
				FROM AppBundle:Driver d
				INNER JOIN AppBundle:Provider p WITH (d.provider = :provider)
				WHERE d.provider = p AND d.driver_id != :driver_excluded ";
        
        $q .= " GROUP BY d.driver_id ";
        
        $qb = $this->getEntityManager()
            ->createQuery($q)
            ->setParameter('provider', $provider)
            ->setParameter('driver_excluded', $driver_excluded);
        
        return $qb->getResult();
    }

    public function paginate($dql, $page = 1, $limit = 5)
    {
        $paginator = new Paginator($dql);
        
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);
        
        return $paginator;
    }
}
