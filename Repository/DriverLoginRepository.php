<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Kkos
 */
class DriverLoginRepository extends EntityRepository
{

    public function getDeviceToken($driver)
    {
        $dql = "SELECT dl.device_token
					FROM AppBundle:DriverLogin dl
					WHERE dl.driver = :driver
					ORDER BY dl.logdate DESC
					";
        
        $result = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('driver', $driver)
            ->getResult();
        
        if (! empty($result))
            return $result[0]['device_token'];
        else
            return null;
    }
}