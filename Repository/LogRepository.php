<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 *
 * @author Kkos
 *        
 */
class LogRepository extends EntityRepository {
	
	public function getAlerts($user_id, $table = null, $type = null) {
		
		$q = "SELECT l.*, u.username
                FROM sx_logs l
				INNER JOIN sx_users as u ON (l.user_id = u.id)
                WHERE l.alert_user = :user_id AND l.alert>0 AND l.alert!=3 ";
		
		if (!empty($type))	$q .= " AND l.alert = :type ";
		if (!empty($table)) $q .= " AND l.table_name = :table  ";
		$q .= " ORDER BY l.log_id DESC LIMIT 10";
		
		$statement = $this->getEntityManager ()->getConnection ()->prepare ( $q );	
		$statement->bindValue ( 'user_id', $user_id );
		
		$statement->execute ();
		return $statement->fetchAll ();
	}
	
	public function getLogs($table, $record_id, $user_id) {
		
		$statement = $this->getEntityManager ()->getConnection ()->prepare ( "
                SELECT l.*, u.username
                FROM sx_logs l
				INNER JOIN sx_users as u ON (l.user_id = u.id) 
                WHERE l.table_name = :table AND l.affected_id = :record_id AND (l.alert = 0 OR l.alert IS NULL)  
                ORDER BY l.log_id DESC
            " );
		
		$statement->bindValue ( 'table', $table );
		$statement->bindValue ( 'record_id', $record_id );
		$statement->execute ();
		return $statement->fetchAll ();
	}
	
	public function getAdminLogs() {
		
		$statement = $this->getEntityManager ()->getConnection ()->prepare ( "
                SELECT l.*, u.username
                FROM sx_logs l
				INNER JOIN sx_users as u ON (l.user_id = u.id)
                WHERE 1=1 GROUP BY l.type, l.affected_id, l.content
                ORDER BY l.log_id DESC LIMIT 200
            " );
		$statement->execute ();
		return $statement->fetchAll ();
	}
}
