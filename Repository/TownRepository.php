<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Town;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * @author Kkos
*/

class TownRepository extends EntityRepository
{
	/**
	 * @return Query
	 */
	public function findLikeName($q)
	{
		
		return $this->getEntityManager()->createQuery("
                SELECT t
                FROM AppBundle:Town t
                WHERE t.name LIKE :searchterm  
                ORDER BY t.name ASC
            ")->setMaxResults(8)->setParameter('searchterm', "%$q%")->getArrayResult();
		
	}

	/**
	 * @param int $page
	 *
	 * @return Pagerfanta
	 */
	public function findLatest($page = 1)
	{
		$paginator = new Pagerfanta(new DoctrineORMAdapter($this->queryLatest(), false));
		$paginator->setMaxPerPage(Post::NUM_ITEMS);
		$paginator->setCurrentPage($page);

		return $paginator;
	}
}
